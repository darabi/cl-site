(in-package :cl-user)

(uiop:with-current-directory (*load-truename*)
  (load "../../build"))

(ql:quickload :cl-site-zaserve)

;;
;; Start zaserve, publish site directory & pages, set default package.
;;
(cl-site-zaserve:start-server)
(cl-site-zaserve:publish-cl-site)
(setq *package* (find-package :cl-site))

(let ((wserver net.aserve:*wserver*))
  (when wserver
    (let ((port (cl-site-paserve:aserve-socket-port (net.aserve:wserver-socket wserver))))
      (format t "You may now visit http://localhost:~a to test the site with zaserve.~%" port))))


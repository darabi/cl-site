;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site-clack
  :name "cl-site-clack"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "Dave Cooper & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Test server for common-lisp.net for Clack"
  :depends-on (:cl-site :clack) 
  :serial t
  :components ((:file "package")
	       (:file "publish")))

